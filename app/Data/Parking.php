<?php namespace App\Data;
 
use Illuminate\Database\Eloquent\Model;
 
class Parking extends Model
{
    
     protected $fillable = ['name', 'latitude', 'longitude', 'price', 'counts', 'timings', 'detailedPrices'];
    
}