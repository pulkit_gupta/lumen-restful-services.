<?php
 
namespace App\Http\Controllers;
 
use App\Data\Parking;
use App\Http\Utility\Utils;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 
/**
*   Class containing RESTAPI
*   Author: Pulkit Gupta
*/ 
class ParkingController extends Controller{


    /**
    *   @param Request
    *   Get request to get array of all parking spaces (Optional query param: lat and long)
    *   @return array
    */
    public function index(Request $request){

        $Parkings  = Parking::all();

        if($request->has('lat')&&$request->has('long')){
            foreach ($Parkings as $park) {
                $park->distance = Utils::getDistance($park->latitude,$park->longitude,$request->input('lat'), $request->input('long'),"K");
            }    
        }
        
        return response()->json($Parkings);

    }
 
    /**
    *   @param id, Request
    *   Get request to get array of parking spaces with $id (Optional query param: lat and long)
    *   @return array
    */
    public function getParking($id, Request $request){
 
        $Parking  = Parking::find($id);
       
        if($request->has('lat')&&$request->has('long')){
           $Parking->distance = Utils::getDistance($Parking->latitude,$Parking->longitude,$request->input('lat'), $request->input('long'),"K"); 
        }
        
        return response()->json($Parking);
    }
 
    public function createParking(Request $request){
 
        $Parking = Parking::create($request->all());
 
        return response()->json($Parking);
 
    }
 
    public function deleteParking($id){
        $Parking  = Parking::find($id);
        $Parking->delete();

        return response()->json('success');
    }
 
    public function updateParking(Request $request,$id){
        $Parking  = Parking::find($id);
        $Parking->name = $request->input('name');
        $Parking->latitude = $request->input('latitude');
        $Parking->longitude = $request->input('longitude');
        $Parking->price = $request->input('price');
        $Parking->counts = intval($request->input('counts'));
        $Parking->timing = $request->input('timing');
        $Parking->detailedPrices = $request->input('detailedPrices');
        $Parking->save();
        
        return response()->json($Parking);
    }
 
}