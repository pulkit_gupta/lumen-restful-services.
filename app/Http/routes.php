<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->get('/', function() use ($app) {
    return "Park Pocket API server";
});


$app->group(['prefix' => 'api','namespace' => 'App\Http\Controllers'], function($app)
{
    $app->get('parking','ParkingController@index');
 
	$app->get('parking/{id}','ParkingController@getParking');
	 
	$app->post('parking','ParkingController@createParking');
	 
	$app->put('parking/{id}','ParkingController@updateParking');
	 
	$app->delete('parking/{id}','ParkingController@deleteParking');
});

