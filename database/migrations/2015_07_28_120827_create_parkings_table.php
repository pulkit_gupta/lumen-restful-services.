<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parkings', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->string('name');
			$table->string('latitude');
			$table->string('longitude');
			$table->string('price');
			$table->integer('counts');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('parkings');
	}

}
